package com.oreillyauto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.domain.User;
import com.oreillyauto.domain.foo.Pupil;

public class Response implements Serializable {
	private static final long serialVersionUID = 7092938701510329645L;

	public Response() {
	}

	private String message;
	private String messageType;
	private List<User> userList = new ArrayList<User>();
	private List<Pupil> pupilList = new ArrayList<Pupil>();
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<Pupil> getPupilList() {
		return pupilList;
	}

	public void setPupilList(List<Pupil> pupilList) {
		this.pupilList = pupilList;
	}
	
}
