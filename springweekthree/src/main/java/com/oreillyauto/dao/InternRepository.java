package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.InternRepositoryCustom;
import com.oreillyauto.domain.interns.Intern;

public interface InternRepository extends CrudRepository<Intern, Integer>, InternRepositoryCustom {
    
}
