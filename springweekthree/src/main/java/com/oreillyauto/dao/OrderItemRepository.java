package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.OrderItemRepositoryCustom;
import com.oreillyauto.domain.orders.OrderItem;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer>, OrderItemRepositoryCustom {
    
}
