package com.oreillyauto.domain.university;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "University")
public class University implements Serializable {
    private static final long serialVersionUID = 5720901624513316319L;

    @Id
    @Column(name = "university_id")
    private Integer universityId;

    @Column(name = "university_name")
    private Integer universityName;

    //@JsonBackReference
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "course_id")
    private Course course;

	public Integer getUniversityId() {
		return universityId;
	}

	public void setUniversityId(Integer universityId) {
		this.universityId = universityId;
	}

	public Integer getUniversityName() {
		return universityName;
	}

	public void setUniversityName(Integer universityName) {
		this.universityName = universityName;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "University [universityGuid=" + universityId + ", universityName=" + universityName + "]";
	}

}
