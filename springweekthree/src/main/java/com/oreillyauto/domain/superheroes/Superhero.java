package com.oreillyauto.domain.superheroes;

public class Superhero {
    
    public Superhero() {};    
    
    public Superhero(String heroName) {
        super();
        this.heroName = heroName;
    }


    private String heroName;
    
    
    public String getHeroName() {
        return heroName;
    }
    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }
    
    
    @Override
    public String toString() {
        return "Superhero [heroName=" + heroName + "]";
    }
    
    

}
