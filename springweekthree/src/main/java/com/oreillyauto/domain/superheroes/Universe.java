package com.oreillyauto.domain.superheroes;

public class Universe {

    public Universe() {};
    public Universe(String name, Team team) {
        super();
        this.name = name;
        this.team = team;
    }
    
    
    private String name;
    private Team team;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Team getTeam() {
        return team;
    }
    public void setTeam(Team team) {
        this.team = team;
    }
    
    
    @Override
    public String toString() {
        return "Universe [name=" + name + ", team=" + team + "]";
    }
    
    

}
