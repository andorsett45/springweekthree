package com.oreillyauto.domain.superheroes;

import java.util.Arrays;

public class Team {
    
    public Team() {};
    
    public Team(String teamName, Superhero[] superheroes) {
        super();
        this.teamName = teamName;
        this.superheroes = superheroes;
    }

    private String teamName;
    private Superhero[] superheroes;
    
    
    public String getTeamName() {
        return teamName;
    }
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
    public Superhero[] getSuperheroes() {
        return superheroes;
    }
    public void setSuperheroes(Superhero[] superheroes) {
        this.superheroes = superheroes;
    }
    
    
    @Override
    public String toString() {
        return "Team [teamName=" + teamName + ", superheroes=" + Arrays.toString(superheroes) + "]";
    }
    

    
}
