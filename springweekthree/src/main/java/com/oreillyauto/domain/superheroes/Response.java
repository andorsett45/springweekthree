package com.oreillyauto.domain.superheroes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    public Response() {} 
    
    @JsonCreator
    public Response(@JsonProperty("Universe") Universe[] universe) {
        this.universe = universe;
    }
    
    @JsonProperty("Universe")
    private Universe[] universe;// what we’ll use on our end


    public Universe[] getUniverse() {
        return universe;
    }

    public void setUniverse(Universe[] universe) {
        this.universe = universe;
    }


    

    
    
}   

