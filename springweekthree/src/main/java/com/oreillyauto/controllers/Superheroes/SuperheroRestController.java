package com.oreillyauto.controllers.Superheroes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.oreillyauto.domain.superheroes.Superhero;
import com.oreillyauto.domain.superheroes.Team;
import com.oreillyauto.domain.superheroes.Universe;
import com.oreillyauto.domain.superheroes.Response;
import com.oreillyauto.service.MappingsService;
import com.oreillyauto.util.RestHelper;

@RestController
public class SuperheroRestController {

    @Autowired
    MappingsService mappingService;

    @GetMapping(value = "/rest/superheroes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Universe> getAllUniverses() {
        return mappingService.getUniverse();
    }

    @PostMapping(value = "/rest/superheroes/v1", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAllUniversesV1(@RequestHeader(value = "Authorization") String authString) {

        if (!RestHelper.isUserAuthenticated(authString)) {
            return "{\"error\":\"User not authenticated\"}";
        }
        
        List<Superhero> superHeroList = new ArrayList<Superhero>();
        
        superHeroList.add(new Superhero("Iron Man"));
        superHeroList.add(new Superhero("Hulk"));
        superHeroList.add(new Superhero("Black Widow"));
        superHeroList.add(new Superhero("Thor"));
        Superhero[] superheroes = superHeroList.toArray(new Superhero[superHeroList.size()]);
    
        Team team = new Team("Avengers", superheroes);
    
        Universe univ = new Universe("Marvel", team);
        Universe[] universe = { univ };
        
        Response response = new Response(universe);
        
        return response;
    }
    
    /*public static void main(String[] args) {
        List<Superhero> superHeroList = new ArrayList<Superhero>();
    
        superHeroList.add(new Superhero("Iron Man"));
        superHeroList.add(new Superhero("Hulk"));
        superHeroList.add(new Superhero("Black Widow"));
        superHeroList.add(new Superhero("Thor"));
        Superhero[] superheroes = superHeroList.toArray(new Superhero[superHeroList.size()]);
    
        Team team = new Team("Avengers", superheroes);
    
        Universe univ = new Universe("Marvel", team);
        Universe[] universe = { univ };
        
        Response response = new Response(universe);
        
        
    }*/

}


