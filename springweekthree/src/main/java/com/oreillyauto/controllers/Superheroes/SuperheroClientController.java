package com.oreillyauto.controllers.Superheroes;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oreillyauto.domain.superheroes.Response;
import com.oreillyauto.domain.superheroes.Universe;
import com.oreillyauto.service.MappingsService;



@Controller
public class SuperheroClientController {

    @Autowired
    MappingsService mappingService;
    
    /**
     * I don't know that I like using a jsonTemplate configuration. The
     * result is always an Object {} that holds the information. Instead 
     * of returning an array of Intern Data:
     *   [{<Intern>}, {<Intern>}]
     * the code below returns:
     *   { "InternList" : [{<Intern>}, {<Intern>}] }
     * @param model
     * @return
     */
    @GetMapping(value = "/rest/superheroesTemplate")
    public String getAllUniversesTemplate(Model model) {
        List<Universe> universeList = mappingService.getUniverse();
        model.addAttribute("universeList", universeList);
        return "jsonTemplate";
    }
    

@ResponseBody
@GetMapping(value = { "/rest/testSuperheroRestv1" })
public String getTestAuthenticatedRestServiceV1(HttpServletRequest req)  {
    // Logic in the controller! Argh! Training Purposes Only!
    
    // Define the server uri, context path, and endpoint
    String url = "http://localhost:8080" + req.getContextPath() + "/rest/superheroes/v1";
    
    // Setup authentication and encode it
    String auth = "cGFzc3dvcmQ=";
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
    
    // Create Request Headers
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.set("Authorization", "Basic " + new String(encodedAuth));

    // Create Request Body (Payload) (if applicable)
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

    // Send The Request to the Web Service and Print the Response
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Response> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Response.class); //deserializing
        Response response = responseEntity.getBody(); //pull back the object from the responseEntity
        System.out.println(new Gson().toJson(response)); //convert back to json

        return "done";
}

}

