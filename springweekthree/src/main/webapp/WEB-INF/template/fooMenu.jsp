<%@ include file="/WEB-INF/layouts/include.jsp"%>

<nav class="navbar bg-light">
	<ul class="nav nav-pills flex-column">
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/foo' />">
		    	New Student
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/foo/report' />">
		    	Course Report
		    </a>
		</li>
	</ul>
</nav>